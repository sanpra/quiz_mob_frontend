// import action types
import {
  QUIZ_METADATA,
  ADD_QUIZ_QUESTIONS,
  STORE_ANSWER,
  CLEAR_ANSWERS,
} from '../store/actionTypes';

const initialState = {
  quiz: [],
  activeQuiz: {},
  totalPages: 1,
  answers: [],
};

const QuizReducer = (state = initialState, action) => {
  switch (action.type) {
    case QUIZ_METADATA:
      return {
        ...state,
        quiz: action.payload.quiz,
        totalPages: action.payload.totalPages,
      };
    case ADD_QUIZ_QUESTIONS:
      return { ...state, activeQuiz: action.payload };

    case STORE_ANSWER:
      let flag = false;

      state.answers.forEach(answer => {
        if (answer.questionId == action.payload.questionId) {
          answer.answer = action.payload.answer;
          flag = true;
        }
      });
      if (!flag) {
        return { ...state, answers: [...state.answers, action.payload] };
      } else {
        return { ...state, answers: state.answers };
      }

    case CLEAR_ANSWERS:
      return { ...state, answers: [] };

    case CLEAR_ANSWERS:
      return { ...state, answers: [] };
    default:
      return { ...state };
  }
};

export default QuizReducer;
