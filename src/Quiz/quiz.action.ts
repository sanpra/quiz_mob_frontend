import axios from 'axios';
import { RECORDS_PER_PAGE } from '../utils/config';
import { URL } from '../utils/routeConstants';
import { QUIZ_METADATA } from '../store/actionTypes';
import axiosInstance from '../Axios/axiosInterceptor';

export const getQuiz = async (currentPage: number, title: string) => {
  let response;
  if (!title) {
    title = '';
  }
  await axiosInstance
    .get(
      `${URL}/quiz?pageNumber=${currentPage}&recordsPerPage=${RECORDS_PER_PAGE}&title=${title}`
    )
    .then(res => {
      response = res;
    })
    .catch(err => {
      throw err;
    });

  return response;
};

export const storeQuiz = fetchedQuiz => {
  return {
    type: QUIZ_METADATA,
    payload: fetchedQuiz,
  };
};
