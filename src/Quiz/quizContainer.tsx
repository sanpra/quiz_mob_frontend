import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Container,
  Content,
  Header,
  Title,
  Toast,
  Text,
  Right,
  Icon,
  Button,
  Item as FormItem,
  Input,
  Spinner,
} from 'native-base';
import { connect } from 'react-redux';
import { QuizQuestions, Auth, Answers } from '../Routes/routes';
import { debounce } from 'lodash';
import { i18N } from '../utils/i18N';
import { MIN_SEARCH_TEXT_LENGTH, DEFAULT_PAGE } from '../utils/config';

import QuizCard from '../components/cardComponent';

// actions
import { getQuiz, storeQuiz } from './quiz.action';
import { clearStore } from '../Auth/auth.action';
import theme from '../styles/theme.styles';

interface Props {
  authUser: string;
  quiz: any;
  totalPages: number;
  navigation: any;
  storeQuiz: (object) => void;
  clearStore: () => void;
}

interface State {
  data: any;
  totalPages: number;
  currentPage: number;
  showSearchBar: boolean;
  error: boolean;
  errorMessage: string;
  listLoader: boolean;
  searchLoader: boolean;
}

class QuizContainer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.props.navigation.addListener('willFocus', () => {
      this.fetchQuiz(DEFAULT_PAGE);
    });
    this.state = {
      data: this.props.quiz,
      totalPages: 1,
      currentPage: DEFAULT_PAGE,
      showSearchBar: false,
      error: false,
      errorMessage: '',
      listLoader: false,
      searchLoader: false,
    };
  }
  unauthorizedAccess = async () => {
    this.props.navigation.navigate(Auth);
    alert(i18N.messages.logoutMessage);
  };

  fetchQuiz = async (currentPage: number, title?: string) => {
    this.setState({ currentPage: currentPage, listLoader: true });
    let response = await getQuiz(currentPage, title);
    if (!response) {
      this.unauthorizedAccess();
    } else {
      this.setState({ listLoader: false });
      if (response.data.status == 200) {
        if (currentPage == DEFAULT_PAGE) {
          this.setState({
            data: response.data.data.quiz,
            totalPages: response.data.data.totalPages,
          });
          await this.props.storeQuiz(response.data.data);
        } else {
          this.setState({
            data: [...this.state.data, ...response.data.data.quiz],
            totalPages: response.data.data.totalPages,
          });
        }
      } else {
        this.setState({
          error: true,
          errorMessage: response.data.message,
          listLoader: false,
        });
      }
    }
  };

  wrapFetchQuizByName = (title: string) => {
    if (title.length > MIN_SEARCH_TEXT_LENGTH) {
      this.fetchQuizByName(title);
    } else if (title.length < MIN_SEARCH_TEXT_LENGTH) {
      this.setState({ data: null, error: false });
    }
  };
  fetchQuizByName = debounce(async (title: string) => {
    this.setState({ searchLoader: true, data: null, error: false });
    let response = await getQuiz(DEFAULT_PAGE, title);
    if (!response) {
      this.unauthorizedAccess();
    } else {
      this.setState({ searchLoader: false });

      if (response.data.status == 200) {
        this.setState({
          data: response.data.data.quiz,
          totalPages: response.data.data.totalPages,
          error: false,
          // currentPage: 1,
        });
      } else {
        this.setState({
          error: true,
          errorMessage: response.data.message,
          listLoader: false,
        });
      }
    }
  }, 1000);

  goNext = id => {
    this.props.navigation.push(QuizQuestions, { id: id });
  };

  closeSearchBar = () => {
    this.setState({
      ...this.state,
      data: this.props.quiz,
      showSearchBar: false,
      totalPages: this.props.totalPages,
      error: false,
    });
  };

  navigateToAnswer = (quizId: string) => {
    this.props.navigation.push(Answers, { quizId });
  };

  render() {
    return (
      <Container style={styles.rootContainer}>
        <Header style={[styles.header]}>
          {this.state.showSearchBar ? (
            <FormItem underline>
              <Input
                placeholder="Search quiz"
                placeholderTextColor="#ddd"
                style={styles.textInput}
                onChangeText={input => this.wrapFetchQuizByName(input)}
              />
              <Icon
                name="close"
                android="md-close"
                style={styles.iconColor}
                onPress={() => this.closeSearchBar()}
              />
            </FormItem>
          ) : (
            <Title style={[styles.headerText]}>{i18N.headers.appName}</Title>
          )}

          <Right>
            {/* OPEN SEARCH BAR BUTTON */}
            {!this.state.showSearchBar && (
              <Button
                transparent
                onPress={() =>
                  this.setState({
                    showSearchBar: true,
                    data: null,
                    totalPages: 1,
                  })
                }
              >
                <Icon
                  name="search"
                  android="md-search"
                  color={theme.ICON_LIGHT}
                />
              </Button>
            )}
          </Right>
        </Header>

        {this.state.error ? (
          <View style={styles.errorBlock}>
            <Text>{this.state.errorMessage}</Text>
          </View>
        ) : this.state.searchLoader ? (
          <Spinner color={theme.SECONDARY} />
        ) : (
          <View style={styles.mainContainer}>
            <QuizCard
              quizData={this.state.data}
              goNext={this.goNext}
              totalPages={this.state.totalPages}
              currentPage={this.state.currentPage}
              fetchMore={this.fetchQuiz}
              isLoading={this.state.listLoader}
              closeSearchBar={this.closeSearchBar}
              navigateToAnswer={this.navigateToAnswer}
            />
          </View>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  rootContainer: {
    backgroundColor: theme.BACKGROUND_LIGHT,
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 10,
  },
  header: {
    backgroundColor: theme.PRIMARY,
    alignItems: 'center',
  },
  headerText: {
    // fontSize: 24,
    // letterSpacing: 10,
  },
  iconColor: {
    color: theme.ICON_LIGHT,
  },
  textInput: {
    color: theme.TEXT_LIGHT,
  },
  errorBlock: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = state => ({
  authUser: state.auth.authUser._id,
  quiz: state.quiz.quiz,
  totalPages: state.quiz.totalPages,
});
const mapDispatchToProps = {
  storeQuiz,
  clearStore,
};

export default connect(mapStateToProps, mapDispatchToProps)(QuizContainer);
