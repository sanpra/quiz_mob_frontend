import { MIN_PASSWORD_LENGTH } from '../utils/config';
import { i18N } from '../utils/i18N';

export const validateEmail = (value: string) => {
  const emailRegEx = /[a-zA-Z._]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)/; ///^[a-zA-Z]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(rishabhsoft)\.com$/;
  let valid = emailRegEx.test(value) ? true : false;
  return valid;
};
export const validatePassword = (value: string) => {
  let regex;
  if (value.length == 0) {
    return { valid: false, message: i18N.messages.passwordRequired };
  }
  regex = /[A-Z]/;
  if (!regex.test(value)) {
    return { valid: false, message: i18N.messages.uppercaseRequired };
  }
  regex = /[a-z]/;
  if (!regex.test(value)) {
    return { valid: false, message: i18N.messages.lowercaseRequired };
  }
  regex = /[0-9]/;
  if (!regex.test(value)) {
    return { valid: false, message: i18N.messages.numberRequired };
  }
  regex = /[\s]/;
  if (regex.test(value)) {
    return {
      valid: false,
      message: i18N.messages.noWhiteSpace,
    };
  }
  regex = /[\W]/;
  if (!regex.test(value)) {
    return {
      valid: false,
      message: i18N.messages.spacialCharacterRequired,
    };
  }
  if (value.length < MIN_PASSWORD_LENGTH) {
    return {
      valid: false,
      message: i18N.messages.minimumPasswordLength(MIN_PASSWORD_LENGTH),
    };
  }
  return { valid: true };
};

export const validateDomain = (email: string) => {
  const domain = email.substring(email.indexOf('@'));
  const domainNames = ['@rishabhsoft.com'];
  let flag = false;
  domainNames.forEach(domainName => {
    if (domainName === domain) flag = true;
  });
  return flag;
};

export const validateName = (name: string) => {
  const nameRegEx = /^[a-zA-Z-^~]{1,50}$/;
  let valid = nameRegEx.test(name) ? true : false;
  return valid;
};
