export const RECORDS_PER_PAGE = 7;
export const HOSTNAME = '172.16.9.54';
export const PORT = 8000;
export const MIN_SEARCH_TEXT_LENGTH = 2;

export const DEFAULT_PAGE = 1;
export const SECONDS = 60;
export const TIME_LEFT = 1;

export const OPTION_CHAR_CODE = 97;
export const MIN_PASSWORD_LENGTH = 8;
