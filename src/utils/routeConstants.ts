import { HOSTNAME, PORT } from './config';

export const URL = `http://${HOSTNAME}:${PORT}`;
export const URL_USER = 'user';
export const REGISTER_USER = 'register';
