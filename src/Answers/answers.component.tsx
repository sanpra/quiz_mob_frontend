import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import theme from '../styles/theme.styles';
import { OPTION_CHAR_CODE } from '../utils/config';
import { Icon } from 'native-base';

interface Props {
  data: Array<any>;
}

export default function AnswersComponent(props: Props) {
  let charCode;
  const nextCharCode = index => {
    if (index == 0) {
      charCode = OPTION_CHAR_CODE;
    }
    return charCode++;
  };

  const Answer = (item, index) => {
    return (
      <View style={styles.rootContainer}>
        <View style={styles.questionContainer}>
          <Text style={[styles.questionText, styles.bold]}>{index + 1}. </Text>
          <View style={styles.questionTextContainer}>
            <Text style={[styles.questionText, styles.bold]}>{item.title}</Text>
          </View>
        </View>
        {item.options.map((option, index) => (
          <View style={styles.optionContainer} key={option._id}>
            <Text style={styles.optionIndexContainer}>
              {String.fromCharCode(nextCharCode(index))}.
            </Text>
            <View style={styles.optionTextContainer}>
              <Text
                style={
                  option.isCorrect
                    ? [styles.correctAnswer, styles.bold]
                    : option.userSelected
                    ? [styles.incorrectAnswer, styles.bold]
                    : styles.optionText
                }
              >
                {option.text}
              </Text>
            </View>
            {option.userSelected && (
              <Icon name="md-person" style={styles.userIcon} />
            )}
          </View>
        ))}
      </View>
    );
  };

  return (
    <FlatList
      data={props.data}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item, index }) => Answer(item, index)}
    />
  );
}

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  questionContainer: {
    paddingVertical: 10,
    flexDirection: 'row',
  },
  questionTextContainer: {
    flex: 1,
  },
  optionIndexContainer: {
    paddingHorizontal: 10,
  },
  optionTextContainer: {
    flex: 1,
  },
  optionContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
    borderRadius: 25,
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: '#eee',
  },
  questionText: {
    fontSize: 18,
  },
  correctAnswer: {
    color: theme.SUCCESS,
  },
  incorrectAnswer: {
    color: theme.FAILED,
  },
  bold: {
    fontWeight: 'bold',
  },
  optionText: {
    color: theme.TEXT_DARK,
  },
  userIcon: {
    fontSize: 14,
  },
});
