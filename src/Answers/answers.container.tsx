import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Container,
  Header,
  Left,
  Right,
  Button,
  Icon,
  Title,
  Body,
  Spinner,
  Text,
  Content,
} from 'native-base';
//component
import AnswersComponent from './answers.component';
//actions
import { getResult } from './answers.action';
import { Auth } from '../Routes/routes';
import { i18N } from '../utils/i18N';
import theme from '../styles/theme.styles';
interface Props {
  navigation: any;
}

interface State {
  result: any;
  error: boolean;
  errorMessage: string;
  loader: boolean;
}

class AnswersContainer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      result: [],
      error: false,
      errorMessage: '',
      loader: false,
    };
  }

  componentDidMount() {
    this.fetchResult();
  }

  async fetchResult() {
    this.setState({ loader: true });
    let response = await getResult(this.props.navigation.getParam('quizId'));
    if (!response) {
      this.setState({ loader: false });
      this.props.navigation.navigate(Auth);
      alert(i18N.messages.logoutMessage);
    } else {
      if (response.data.status === 200) {
        await this.setState({
          result: response.data.data.result,
          loader: false,
        });
      } else {
        this.setState({
          error: true,
          errorMessage: response.data.message,
          loader: false,
        });
      }
    }
  }
  render() {
    const { navigation } = this.props;
    const { loader, error, errorMessage } = this.state;
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button
              transparent
              onPress={() => {
                navigation.pop();
              }}
            >
              <Icon name="arrow-back" style={styles.goBackIcon} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.headerText}>{i18N.labels.answers}</Title>
          </Body>
          <Right />
        </Header>
        <View style={styles.mainContainer}>
          {loader ? (
            <Spinner color={theme.SECONDARY} />
          ) : error ? (
            <View style={styles.errorContainer}>
              <Text>{errorMessage}</Text>
            </View>
          ) : (
            <AnswersComponent data={this.state.result} />
          )}
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: theme.BACKGROUND_LIGHT,
    alignItems: 'center',
  },
  goBackIcon: {
    color: theme.PRIMARY,
    fontSize: 30,
  },
  headerText: {
    color: theme.PRIMARY,
    fontSize: 25,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
  },
  mainContainer: {
    flex: 1,
    backgroundColor: theme.BACKGROUND_LIGHT,
    paddingHorizontal: 10,
  },
  errorContainer: {
    paddingVertical: 50,
    paddingHorizontal: 10,
  },
});
export default AnswersContainer;
