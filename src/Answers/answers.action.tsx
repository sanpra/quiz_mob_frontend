import { URL } from '../utils/routeConstants';
import axiosInstance from '../Axios/axiosInterceptor';

export const getResult = async (quizId: string) => {
  let response;
  await axiosInstance
    .get(`${URL}/result/${quizId}`)
    .then(res => {
      response = res;
    })
    .catch(err => {
      throw err;
    });

  return response;
};
