import { AsyncStorage } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

// import routename constants
import { Auth, App } from './routes';

// import stacks
import authStack from '../Routes/authStack';
import AppStack from './AppStack';

export const rootNavigator = (signedIn = 'false') => {
  return createAppContainer(
    createSwitchNavigator(
      {
        Auth: {
          screen: authStack,
        },
        App: {
          screen: AppStack,
        },
      },
      {
        initialRouteName: signedIn === 'true' ? App : Auth,
      }
    )
  );
};
// const AppNavigator = createAppContainer(rootNavigator);

// export default AppNavigator;
