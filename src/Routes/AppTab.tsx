import React from 'react';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { Ionicons } from 'react-native-vector-icons';
import theme from '../styles/theme.styles';
// import screens
import QuizContainer from '../Quiz/quizContainer';
import ProfileContainer from '../Profile/profileContainer';

const AppBottomTab = createMaterialBottomTabNavigator(
  {
    Quiz: {
      screen: QuizContainer,
      navigationOptions: {
        tabBarLabel: 'Quiz',
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-list-box" color={tintColor} size={25} />
        ),
      },
    },
    Profile: {
      screen: ProfileContainer,
      navigationOptions: {
        tabBarLabel: 'Profile',
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-contact" color={tintColor} size={25} />
        ),
      },
    },
  },
  {
    initialRouteName: 'Quiz',
    barStyle: {
      backgroundColor: theme.PRIMARY,
    },
    activeColor: theme.SECONDARY,
  }
);

export default AppBottomTab;
