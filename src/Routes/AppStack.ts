import { createStackNavigator } from 'react-navigation-stack';
import { BottomNavigator } from './routes';
// screens and components
import AppBottomTab from './AppTab';
import QuestionsContainer from '../Questions/questions.container';
import AnswersContainer from '../Answers/answers.container';

const AppStack = createStackNavigator(
  {
    BottomNavigator: {
      screen: AppBottomTab,
    },
    QuizQuestions: {
      screen: QuestionsContainer,
    },
    Answers: {
      screen: AnswersContainer,
    },
  },
  {
    initialRouteName: BottomNavigator,
    defaultNavigationOptions: {
      headerShown: false,
    },
  }
);

export default AppStack;
