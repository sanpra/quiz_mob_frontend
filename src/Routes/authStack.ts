import { createStackNavigator } from 'react-navigation-stack';

// import screens
import LoginContainer from '../Login/loginContainer';
import RegisterContainer from '../Register/registerContainer';
import ForgotPasswordContainer from '../forgotPassword/forgotPasswordContainer';

const authStack = createStackNavigator(
  {
    Login: {
      screen: LoginContainer,
      navigationOptions: {
        headerShown: false,
      },
    },
    Register: {
      screen: RegisterContainer,
    },
    ForgotPassword: {
      screen: ForgotPasswordContainer,
    },
  },
  {
    initialRouteName: 'Login',
    defaultNavigationOptions: {
      headerShown: false,
    },
  }
);

export default authStack;
