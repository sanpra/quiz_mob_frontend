import { combineReducers } from 'redux';
import { CLEAR_SESSION } from './actionTypes';
// import reducers
import authReducer from '../Auth/auth.reducer';
import QuizReducer from '../Quiz/quiz.reducer';

const appReducer = combineReducers({
  auth: authReducer,
  quiz: QuizReducer,
});

const rootReducer = (state, action) => {
  if (action.type === CLEAR_SESSION) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
