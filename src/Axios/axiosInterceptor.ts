import axios from 'axios';
import store from '../store';
import { AsyncStorage } from 'react-native';

// actions
import { logout, clearStore } from '../Auth/auth.action';

const getToken = async () =>
  await AsyncStorage.getItem('token')
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });

const axiosInstance = axios.create({});

axiosInstance.interceptors.request.use(async config => {
  config.headers.Authorization = 'Bearer ' + (await getToken());
  return config;
});

axiosInstance.interceptors.response.use(res => {
  console.log('From response interceptor =', res);
  if (res.data.status == 403 || res.data.status == 401) {
    AsyncStorage.setItem('LoggedIn', 'false');
    AsyncStorage.setItem('token', null);
    const state = store.getState();
    const authUserId = state.auth.authUser._id;
    logout(authUserId);
    store.dispatch(clearStore());
    res = null;
  }
  return res;
});

export default axiosInstance;
