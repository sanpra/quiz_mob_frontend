import React from 'react';
import { Toast } from 'native-base';
// connect redux store
import { connect } from 'react-redux';
// Login component
import LoginComponent from './loginComponent';
//route names
import { App, Register, ForgotPassword } from '../Routes/routes';
// actions
import { storeAuthUser, authUser } from '../Auth/auth.action';

import { AsyncStorage } from 'react-native';

interface Props {
  navigation: any;
  storeAuthUser: (object) => void;
}

class LoginContainer extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  login = async (email: string, password: string) => {
    let response = await authUser(email, password);
    if (response.data.status == 200) {
      Toast.show({ text: response.data.message, type: 'success' });
      await this.props.storeAuthUser(response.data.data);
      await AsyncStorage.setItem('LoggedIn', 'true');
      await AsyncStorage.setItem('token', response.data.data.accessToken);
      await AsyncStorage.setItem('AuthUserId', response.data.data._id);

      this.props.navigation.navigate(App);
    } else if (response.data.status == 401) {
      Toast.show({ text: response.data.message, type: 'danger' });
    } else {
      Toast.show({ text: response.data.message, type: 'danger' });
    }
  };

  register = () => {
    this.props.navigation.push(Register);
  };

  forgotPassword = () => {
    this.props.navigation.push(ForgotPassword);
  };
  render() {
    return (
      <LoginComponent
        login={this.login}
        openRegisterPage={this.register}
        openForgotPassword={this.forgotPassword}
      />
    );
  }
}

const mapStateToProps = state => ({
  user: state.authUser,
});
const mapDispatchToProps = {
  storeAuthUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
