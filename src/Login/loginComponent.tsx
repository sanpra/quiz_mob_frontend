import React, { useState } from 'react';
import { StyleSheet, View, Dimensions, Image } from 'react-native';
import {
  Root,
  Container,
  Header,
  Content,
  Text,
  Form,
  Item,
  Input,
  Button,
  Footer,
  Icon,
  Toast,
} from 'native-base';
import theme from '../styles/theme.styles';

// UI constants
import { i18N } from '../utils/i18N';

// Validations
import {
  validateEmail,
  validateDomain,
  validatePassword,
} from '../common/validations';

interface Props {
  login: (email: string, password: string) => void;
  openRegisterPage: () => void;
  openForgotPassword: () => void;
}

interface Fields {
  value: string;
  errorStatus: boolean;
  errorMessage: string;
  securePassword?: boolean;
}

export default function LoginComponent(props: Props) {
  const [email, setEmail] = useState<Fields>({
    value: '',
    errorStatus: false,
    errorMessage: i18N.messages.requireField('Email'),
  });

  const [password, setPassword] = useState<Fields>({
    value: '',
    errorStatus: false,
    errorMessage: i18N.messages.requireField('Password'),
    securePassword: true,
  });

  const validate = (key: string, value: string) => {
    switch (key) {
      case 'email':
        let validEmail = validateEmail(value);
        if (validEmail) {
          let validDomain = validateDomain(value);
          if (validDomain) {
            setEmail({ ...email, value: value, errorStatus: false });
          } else {
            setEmail({
              ...email,
              errorStatus: true,
              errorMessage: i18N.messages.invalidField('Domain'),
            });
          }
        } else {
          setEmail({
            ...email,
            errorStatus: true,
            errorMessage: i18N.messages.invalidField('Email'),
          });
        }
        break;
      case 'password':
        if (value.trim().length >= 8) {
          setPassword({ ...password, value: value, errorStatus: false });
        } else {
          setPassword({
            ...password,
            errorStatus: true,
            errorMessage:
              value.length < 1
                ? i18N.messages.requireField('Password')
                : i18N.messages.invalidField('Password'),
          });
        }
        break;
      default:
        return false;
    }
  };

  const onloginValidation = () => {
    if (email.value == '' && password.value == '') {
      setEmail({
        ...email,
        errorStatus: true,
        errorMessage: i18N.messages.requireField('Email'),
      });
      setPassword({
        ...password,
        errorStatus: true,
        errorMessage: i18N.messages.requireField('Password'),
      });
    } else if (
      !email.errorStatus &&
      !password.errorStatus &&
      email.value !== '' &&
      password.value !== ''
    ) {
      props.login(email.value, password.value);
    } else {
      Toast.show({ text: i18N.messages.emptyForm });
    }
  };

  return (
    <Container>
      <Header style={styles.header} />
      <Content>
        <View style={styles.mainContainer}>
          <View style={styles.logoContainer}>
            <Image
              source={require('../Assets/Logo/rqs_logo.png')}
              // style={[styles.logoImage]}
            />
            {/* <Text style={styles.logoText}>Quiz Board</Text> */}
          </View>
          <Form style={styles.formContainer}>
            <Item style={styles.textInput}>
              <Input
                onChangeText={email => validate('email', email)}
                placeholder="Email"
                placeholderTextColor={theme.PLACEHOLDER}
                autoCorrect={false}
                keyboardType="email-address"
                returnKeyType={'next'}
                returnKeyLabel={'Next'}
              />
            </Item>
            {email.errorStatus && (
              <Text style={styles.error}>{email.errorMessage}</Text>
            )}

            <Item style={styles.textInput}>
              <Input
                secureTextEntry={password.securePassword}
                onChangeText={password => validate('password', password)}
                placeholder="Password"
                placeholderTextColor={theme.PLACEHOLDER}
                onSubmitEditing={() => this.onloginValidation()}
                onBlur={() =>
                  setPassword({ ...password, securePassword: true })
                }
              />
              <Icon
                name="eye"
                android={password.securePassword ? 'md-eye-off' : 'md-eye'}
                ios={password.securePassword ? 'ios-eye-off' : 'ios-eye'}
                onPress={() =>
                  setPassword({
                    ...password,
                    securePassword: !password.securePassword,
                  })
                }
              />
            </Item>
            {password.errorStatus && (
              <Text style={styles.error}>{password.errorMessage}</Text>
            )}

            <Button
              rounded={true}
              info
              block
              style={styles.buttonLogin}
              onPress={() => onloginValidation()}
            >
              <Text>{i18N.labels.loginBtn}</Text>
            </Button>

            <View style={styles.linkContainer}>
              <Button
                danger
                transparent
                onPress={() => props.openForgotPassword()}
              >
                <Text>{i18N.labels.forgotPassword}</Text>
              </Button>
            </View>
          </Form>
        </View>
      </Content>
      <Footer style={styles.footer}>
        <Button info transparent onPress={() => props.openRegisterPage()}>
          <Text>{i18N.labels.register}</Text>
        </Button>
      </Footer>
    </Container>
  );
}

const styles = StyleSheet.create({
  safeAreaContainer: {
    backgroundColor: theme.BACKGROUND_LIGHT,
    flex: 1,
  },
  header: {
    height: 0,
    display: 'none',
  },
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  logoContainer: {
    // flex: 1,
    marginVertical: 15,
    height: Math.round(Dimensions.get('window').height / 3),
    backgroundColor: theme.BACKGROUND_LIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoText: {
    fontSize: 45,
    fontWeight: 'bold',
    color: theme.TEXT_LIGHT,
  },
  formContainer: {
    // flex: 2,
    // height: 400,
    backgroundColor: 'white',
    padding: 15,
    justifyContent: 'center',
  },
  linkContainer: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    padding: 10,
  },
  label: {
    color: theme.PLACEHOLDER,
  },
  buttonLogin: {
    margin: 20,
    marginTop: 30,
    backgroundColor: theme.PRIMARY,
  },
  footer: {
    backgroundColor: theme.BACKGROUND_LIGHT,
  },
  error: {
    color: theme.ERROR,
    paddingLeft: 15,
  },
});
