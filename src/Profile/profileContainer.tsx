import React from 'react';
import { StyleSheet, View, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Button, Text } from 'native-base';

import { logout, clearStore } from '../Auth/auth.action';
import { i18N } from '../utils/i18N';
import { Auth } from '../Routes/routes';
export interface Props {
  navigation: any;
  userId: string;
  clearStore: () => void;
}

export interface State {}

class ProfileContainer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  logout = async () => {
    let response;
    await AsyncStorage.getItem('AuthUserId')
      .then(async id => {
        response = await logout(id);
      })
      .catch(err => console.log(err));
    if (response.data.status === 200) {
      await AsyncStorage.removeItem('token');
      await AsyncStorage.setItem('LoggedIn', 'false');
      await this.props.clearStore();
      this.props.navigation.navigate(Auth);
    } else {
      alert(i18N.messages.logoutError);
    }
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Button onPress={() => this.logout()} rounded>
          <Text>Log Out</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({});

const mapStateToProps = state => ({
  userId: state.auth.authUser._id,
});
const mapDispatchToProps = {
  clearStore,
};
export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);
