import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import {} from 'native-base';
import { connect } from 'react-redux';

export interface Props {
  quiz: any;
}

export interface State {}

class ResultContainer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    // console.log(this.props.quiz);
  }
  render() {
    return (
      <View>
        <Text>Result Compoennt</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({});

const mapStateToProps = state => ({
  quiz: state.quiz,
});
const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps)(ResultContainer);
