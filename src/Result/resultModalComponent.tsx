import React from 'react';
import { Modal, Text, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Icon, Button } from 'native-base';
//interface
import { Result } from '../Questions/questions.container';
import { i18N } from '../utils/i18N';
import theme from '../styles/theme.styles';
interface Props {
  showModal: boolean;
  result: Result;
  closeModal: () => void;
}

export default function ResultModal(props: Props) {
  console.log(props.result);
  const { result } = props;

  return (
    <Modal
      animationType={'slide'}
      transparent={false}
      visible={props.showModal}
    >
      <View style={styles.modal}>
        <TouchableOpacity
          style={styles.btnClose}
          onPress={() => props.closeModal()}
        >
          <Icon
            name="md-close-circle"
            android="md-close-circle"
            ios="ios-close-circle"
          />
        </TouchableOpacity>
        <View style={styles.resultContainer}>
          <View style={styles.statusContainer}>
            <Text style={styles.header}>Result</Text>
            <View>
              <Text
                style={
                  result.status == i18N.resultStatus.Pass
                    ? [styles.text, styles.textGreen]
                    : [styles.text, styles.textRed]
                }
              >
                {result.status}!!
              </Text>
            </View>
          </View>
          <View style={styles.scoreContainer}>
            <View style={styles.alignResult}>
              <Text style={[styles.subHeader, styles.subHeaderBold]}>
                {result.userScore}
              </Text>
              <Text style={styles.subHeader}>{i18N.labels.netScore}</Text>
            </View>
            <View style={styles.alignResult}>
              <Text style={[styles.subHeader, styles.subHeaderBold]}>
                {result.userPercentage}%
              </Text>
              <Text style={styles.subHeader}>{i18N.labels.userPercentage}</Text>
            </View>
          </View>
        </View>
        <View style={styles.btnContainer}>
          <Button
            block
            rounded
            style={styles.btnDone}
            onPress={() => props.closeModal()}
          >
            <Text style={styles.btnText}>{i18N.labels.done}</Text>
          </Button>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    backgroundColor: theme.BACKGROUND_LIGHT,
    padding: 20,
  },
  resultContainer: {
    padding: 20,
  },
  statusContainer: {
    alignItems: 'center',
    height: 150,
    backgroundColor: theme.BACKGROUND_LIGHT,
  },
  scoreContainer: {
    height: 100,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  alignResult: {
    alignItems: 'center',
  },
  header: {
    fontSize: 30,
  },
  subHeader: {
    fontSize: 20,
  },
  subHeaderBold: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  text: {
    fontSize: 35,
    fontWeight: 'bold',
  },
  btnText: {
    color: theme.TEXT_LIGHT,
    fontSize: 18,
  },
  textGreen: {
    color: 'green',
  },
  textRed: {
    color: 'red',
  },
  btnContainer: {
    marginTop: 100,
  },
  btnDone: {
    backgroundColor: theme.PRIMARY,
  },
  btnClose: {
    alignSelf: 'flex-end',
  },
});
