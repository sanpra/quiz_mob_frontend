import React, { useState } from 'react';
import moment from 'moment';
import { capitalize } from 'lodash';
import {
  StyleSheet,
  FlatList,
  View,
  TouchableOpacity,
  Modal,
} from 'react-native';
import theme from '../styles/theme.styles';
import { Card, CardItem, Text, Button, Icon, Spinner } from 'native-base';
import { i18N } from '../utils/i18N';

interface quizData {
  _id: number;
  title: string;
  description: string;
  status: string;
  duration: number;
  passingPercentage: number;
  updatedAt: string;
  result: string;
  endDate: Date;
  userScore?: number;
  userPercentage: number;
}
interface Props {
  quizData: Array<quizData>;
  totalPages: number;
  currentPage: number;
  isLoading: boolean;
  closeSearchBar: () => void;
  goNext: (id) => void;
  fetchMore: (currentPage: number) => any;
  navigateToAnswer: (quizId: string) => void;
}
export default function QuizCard(props: Props) {
  const [showModal, setShowModal] = useState(false);
  const [Modaldata, setModalData] = useState({
    _id: '',
    title: '',
    description: '',
    status: '',
    duration: '',
  });
  const data = props.quizData;
  const showDetails = item => {
    if (item.status === i18N.quizStatus.Available) {
      if (item.result) {
        alert(i18N.messages.alreadyAppeared);
      } else {
        setShowModal(true);
        setModalData(item);
      }
    } else if (item.status == i18N.quizStatus.Expired) {
      props.navigateToAnswer(item._id);
    }
  };

  const startQuiz = id => {
    closeModal();
    props.closeSearchBar();
    props.goNext(id);
  };

  const closeModal = () => {
    setShowModal(false);
  };
  const daysLeft = endDate => {
    let dayLeft = Math.abs(moment(new Date()).diff(moment(endDate), 'days'));
    if (dayLeft >= 7) {
      let weekLeft = Math.abs(
        moment(new Date()).diff(moment(endDate), 'weeks')
      );
      if (weekLeft > 1) {
        return `${weekLeft} ${i18N.labels.weeksLeftUnit}`;
      } else {
        return `${weekLeft} ${i18N.labels.weekLeftUnit}`;
      }
    } else {
      if (dayLeft > 1) {
        return `${dayLeft} ${i18N.labels.daysLeftUnit}`;
      } else {
        return `${dayLeft} ${i18N.labels.dayLeftUnit}`;
      }
    }
  };

  const renderFooter = () => {
    return (
      <View>
        {props.isLoading && <Spinner color={theme.SECONDARY} />}
        {props.totalPages > props.currentPage && !props.isLoading ? (
          <Button
            style={styles.btn}
            rounded
            dark
            transparent
            onPress={() => props.fetchMore(props.currentPage + 1)}
          >
            <Text>{i18N.labels.loadMore}</Text>
            <Icon name="ios-arrow-down" />
          </Button>
        ) : (
          props.totalPages === 0 && (
            <View style={styles.messageContainer}>
              <Text>{i18N.messages.noRecords}</Text>
            </View>
          )
        )}
      </View>
    );
  };

  return (
    <View>
      <Modal animationType={'slide'} transparent={false} visible={showModal}>
        <View style={styles.modal}>
          <View>
            <TouchableOpacity
              style={styles.btnClose}
              onPress={() => closeModal()}
            >
              <Icon
                name="md-close-circle"
                android="md-close-circle"
                ios="ios-close-circle"
              />
            </TouchableOpacity>
            <Text style={[styles.title, styles.titlePadding]}>
              {i18N.labels.title} : {Modaldata.title}
            </Text>
            <Text>{Modaldata.description}</Text>
            <Text style={[styles.subHeader, styles.titlePadding]}>
              {i18N.labels.instructions}
            </Text>
            <FlatList
              data={i18N.messages.instructions(Modaldata.duration)}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <View style={styles.modalContainer}>
                  <Text>
                    {i18N.bullets.rounded} {item.text}
                  </Text>
                </View>
              )}
            />
            <View style={styles.navigationContainer}>
              <Button
                style={styles.btnStartQuiz}
                bordered
                transparent
                onPress={() => startQuiz(Modaldata._id)}
              >
                <Text>{i18N.labels.startQuizBtn}</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>
      <FlatList
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <Card style={styles.card}>
            <TouchableOpacity onPress={() => showDetails(item)}>
              {/* QUIZ STATUS */}
              <View style={styles.cardStatusBar}>
                <CardItem>
                  {item.result && (
                    <Text
                      style={
                        item.result == i18N.resultStatus.Pass
                          ? styles.colorSuccess
                          : styles.colorFailed
                      }
                    >
                      {item.result}
                    </Text>
                  )}
                </CardItem>
                <CardItem style={styles.cardLabel}>
                  {item.status === i18N.quizStatus.Available && (
                    <Text style={[styles.cardLabelText, styles.cardactiveText]}>
                      {item.status}
                    </Text>
                  )}
                  {item.status === i18N.quizStatus.Expired && (
                    <Text
                      style={[styles.cardLabelText, styles.cardexpiredText]}
                    >
                      {item.status}
                    </Text>
                  )}
                </CardItem>
              </View>

              {/* QUIZ TITLE */}
              <CardItem>
                <Text style={styles.title}>{capitalize(item.title)}</Text>
              </CardItem>

              {/* QUIZ FOOTER */}
              {item.status == i18N.quizStatus.Available && !item.result && (
                <CardItem
                  footer
                  style={[styles.cardFooter, styles.cardFooterBackground]}
                >
                  <Icon
                    name="md-time"
                    ios="ios-time-outline"
                    android="md-time"
                  />
                  <Text style={styles.secondarytext}>
                    {i18N.labels.quizDuration} : {item.duration}{' '}
                    {i18N.labels.durationUnit}
                  </Text>
                  <View style={styles.selfAlign}>
                    <Text style={styles.secondarytext}>
                      {i18N.labels.expiresIn}: {daysLeft(item.endDate)}
                    </Text>
                  </View>
                </CardItem>
              )}
              {((item.status === i18N.quizStatus.Available && item.result) ||
                item.status === i18N.quizStatus.Expired) &&
                (item.userScore == null ? (
                  <CardItem
                    footer
                    style={[styles.cardFooterBackground, styles.cardFooter]}
                  >
                    <Text>{i18N.messages.notAppeared}</Text>
                  </CardItem>
                ) : (
                  <CardItem
                    footer
                    style={[styles.cardFooterBackground, styles.cardFooter]}
                  >
                    <Text>
                      {i18N.labels.userScore}: {item.userScore}
                    </Text>
                    <Text>
                      {i18N.labels.userPercentage}: {item.userPercentage}%
                    </Text>
                  </CardItem>
                ))}
            </TouchableOpacity>
          </Card>
        )}
        ListFooterComponent={renderFooter()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    backgroundColor: theme.BACKGROUND_LIGHT,
    padding: 20,
    height: 200,
  },
  modalContainer: {
    paddingVertical: 10,
  },
  messageContainer: {
    alignItems: 'center',
  },
  card: {},
  cardStatusBar: {
    backgroundColor: theme.SECONDARY_INFO,
    flex: 1,
    flexDirection: 'row',
  },
  cardLabel: {
    padding: 0,
    margin: 0,
    flex: 1,
    justifyContent: 'flex-end',
  },
  justifyContent: {
    justifyContent: 'space-between',
  },
  cardLabelText: {
    color: theme.TEXT_LIGHT,
    paddingHorizontal: 10,
  },
  cardactiveText: {
    backgroundColor: theme.SUCCESS,
  },
  cardInUseText: {
    backgroundColor: theme.INFO,
  },
  cardexpiredText: {
    backgroundColor: theme.FAILED,
  },
  btnClose: {
    alignSelf: 'flex-end',
  },
  cardItem: {
    fontSize: 18,
  },
  navigationContainer: {
    // alignSelf: 'flex-end',
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
  },
  titlePadding: {
    paddingVertical: 20,
  },
  subHeader: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  secondarytext: {
    fontSize: 14,
  },
  cardFooter: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  cardFooterBackground: {
    backgroundColor: '#eee',
  },
  selfAlign: {
    flex: 1,
    alignItems: 'flex-end',
  },
  btnStartQuiz: {
    margin: 5,
    alignSelf: 'flex-end',
  },
  btn: {
    justifyContent: 'center',
  },
  colorSuccess: {
    color: theme.SUCCESS,
  },
  colorFailed: {
    color: theme.FAILED,
  },
});
