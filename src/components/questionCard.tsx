import React, { useState } from 'react';
import { StyleSheet, View, Alert, ScrollView } from 'react-native';
import {
  Text,
  Button,
  Icon,
  Container,
  Header,
  Left,
  Right,
  Content,
} from 'native-base';
import CustomProgressBar from './progressBar';
import { RadioButton } from 'react-native-paper';
import CountDown from 'react-native-countdown-component';
import theme from '../styles/theme.styles';
// store
import store from '../store';
//interface
import { Questions } from '../Questions/questions.container';

// actions
import { storeAnswers } from '../Questions/questions.action';
import { i18N } from '../utils/i18N';

import { TIME_LEFT, SECONDS } from '../utils/config';

interface Props {
  questions: Array<Questions>;
  duration: number;
  counter: number;
  updateCounter: (time) => void;
  submit: (timeTaken: number) => void;
  goBack: () => void;
}
export default function QuestionCard(props: Props) {
  const state = store.getState();
  const answers = state.quiz.answers;
  const totalQuestion = props.questions.length;
  // destructors
  const { questions } = props;

  //hooks
  const [index, setIndex] = useState(0);
  const [checked, setChecked] = useState('');
  const [saved, setSaved] = useState(false);
  const [selected, setSelected] = useState(false);
  const [timerRunning, setTimerRunning] = useState(true);

  const previous = () => {
    setIndex(index - 1);
    setSelected(false);
    selectedAnswer(index - 1);
  };

  const next = () => {
    setIndex(index + 1);
    selectedAnswer(index + 1);
    setSelected(false);
    setSaved(false);
  };

  const selectedAnswer = index => {
    answers.forEach(answer => {
      if (answer.questionId === questions[index]._id) {
        setChecked(answer.answer);
      }
    });
  };

  const save = flag => {
    if (flag === i18N.labels.saveAndNext) {
      setIndex(index + 1);
      setSelected(false);
      setSaved(false);
      selectedAnswer(index + 1);
    } else if (flag === i18N.labels.save) {
      setSaved(true);
    } else {
      setSaved(false);
    }
    let payload = {
      questionId: questions[index]._id,
      answer: checked,
    };
    store.dispatch(storeAnswers(payload));
  };

  const submitAlert = async () => {
    if (!saved && selected) {
      save(i18N.labels.submit);
    }

    const state = store.getState();
    const answers = state.quiz.answers;
    Alert.alert(
      i18N.labels.confirmSubmit,
      i18N.messages.totalAttempted(answers.length, totalQuestion),
      [
        {
          text: i18N.labels.yes,
          onPress: () => {
            setTimerRunning(false);
            const timeTaken =
              props.duration - Math.round(props.counter / SECONDS);
            props.submit(timeTaken);
          },
        },
        {
          text: i18N.labels.no,
        },
      ]
    );
  };

  const timerOver = async timeTaken => {
    if (!saved) {
      await save(i18N.labels.submit);
    }
    Alert.alert('', i18N.messages.timeOver, [
      { text: i18N.labels.ok, onPress: () => props.submit(timeTaken) },
    ]);
  };

  const goBack = async () => {
    if (!saved) {
      await save(i18N.labels.submit);
    }
    props.goBack();
  };
  return (
    <Container>
      {/* PROGRESS BAR */}
      <CustomProgressBar progress={answers.length} length={totalQuestion} />

      {/* HEADER - includes timer and back button */}
      <View style={[styles.header]}>
        <Left>
          <Button transparent onPress={() => goBack()}>
            <Icon name="arrow-back" style={styles.goBackIcon} />
          </Button>
        </Left>

        <Right>
          <CountDown
            id="counter"
            until={props.duration * SECONDS}
            onChange={time => {
              if (time === TIME_LEFT * SECONDS) {
                alert(i18N.messages.timerAlert(TIME_LEFT));
              }
              props.updateCounter(time);
            }}
            onFinish={() => timerOver(props.duration)}
            timeLabelStyle={{ color: 'red', fontWeight: 'bold' }}
            timeToShow={props.duration < 60 ? ['M', 'S'] : ['H', 'M', 'S']}
            timeLabels={{ h: null, m: null, s: null }}
            showSeparator
            digitTxtStyle={
              props.counter <= TIME_LEFT * SECONDS
                ? styles.timerDangerText
                : styles.timerText
            }
            digitStyle={styles.timer}
            size={20}
            running={timerRunning}
          />
        </Right>
      </View>

      {/*BODY - Quiz will be rendered in this component */}
      <Content contentContainerStyle={styles.rootContainer}>
        <View style={styles.mainContainer}>
          <View style={styles.questionContainer}>
            <View style={styles.quizInfoContainer}></View>
            <ScrollView>
              <View style={styles.question}>
                <Text style={styles.boldText}>
                  {index + 1}. {questions[index].title}
                </Text>
              </View>
              {/* RENDER OPTIONS */}
              <View>
                {questions[index].options.map((option, index) => (
                  <View style={styles.optionContainer} key={index}>
                    <RadioButton
                      value={option.text}
                      status={checked === option.text ? 'checked' : 'unchecked'}
                      onPress={() => {
                        setChecked(option.text);
                        setSelected(true);
                        setSaved(false);
                      }}
                    />
                    <Text style={styles.optionText}>{option.text}</Text>
                  </View>
                ))}
              </View>
            </ScrollView>
          </View>
          <View style={styles.navigationContainer}>
            {index > 0 && (
              <Button
                style={[styles.btn, styles.btnPrevious]}
                onPress={() => previous()}
                transparent
              >
                <Icon
                  name="ios-arrow-back"
                  android="md-arrow-back"
                  ios="ios-arrow-back"
                />
                <Text style={styles.text}>{i18N.labels.previous}</Text>
              </Button>
            )}
            <View style={styles.alignEnd}>
              {index + 1 < totalQuestion ? (
                !selected ? (
                  <Button success transparent onPress={() => next()}>
                    <Text style={styles.text}>{i18N.labels.next}</Text>
                    <Icon
                      name="md-arrow-forward"
                      android="md-arrow-forward"
                      ios="ios-arrow-forward"
                    />
                  </Button>
                ) : (
                  <Button
                    onPress={() => save(i18N.labels.saveAndNext)}
                    success
                    transparent
                  >
                    <Text style={styles.text}>{i18N.labels.saveAndNext}</Text>
                    <Icon
                      name="md-arrow-forward"
                      android="md-arrow-forward"
                      ios="ios-arrow-forward"
                    />
                  </Button>
                )
              ) : (
                selected && (
                  <Button
                    onPress={() => save(i18N.labels.save)}
                    success
                    transparent
                    disabled={saved}
                    style={styles.btnSave}
                  >
                    {!saved ? (
                      <Text>{i18N.labels.save}</Text>
                    ) : (
                      <Text>{i18N.labels.saved}</Text>
                    )}
                  </Button>
                )
              )}
            </View>
          </View>
          <Button
            block
            warning
            rounded
            onPress={() => submitAlert()}
            style={styles.btnSubmit}
          >
            <Text>{i18N.labels.submit}</Text>
          </Button>
        </View>
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    backgroundColor: theme.BACKGROUND_LIGHT,
  },
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    paddingHorizontal: 15,
    alignItems: 'stretch',
  },
  quizInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  currentQuestionLabel: {
    color: theme.TEXT_LIGHT,
    backgroundColor: theme.LABEL,
    paddingVertical: 5,
    marginVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 10,
  },
  questionContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  question: {
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  navigationContainer: {
    // flex: 2,
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  optionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
    borderRadius: 25,
    paddingHorizontal: 20,
    paddingVertical: 5,
    backgroundColor: '#eee',
  },
  optionText: {
    paddingHorizontal: 10,
  },
  boldText: {
    fontWeight: 'bold',
  },
  btn: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  btnSave: {
    paddingHorizontal: 10,
  },
  btnPrevious: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  text: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  alignEnd: {
    flex: 1,
    alignItems: 'flex-end',
  },
  btnSubmit: {
    marginVertical: 10,
    backgroundColor: theme.PRIMARY,
  },
  timerText: {
    color: theme.TEXT_DARK,
  },
  timerDangerText: {
    color: theme.DANGER,
  },
  timer: {
    backgroundColor: 'transparent',
  },

  header: {
    backgroundColor: theme.BACKGROUND_LIGHT,
    alignItems: 'center',
    flexDirection: 'row',
  },

  headerText: {
    fontSize: 24,
    letterSpacing: 10,
  },
  goBackIcon: {
    color: theme.PRIMARY,
    fontSize: 30,
  },
});
