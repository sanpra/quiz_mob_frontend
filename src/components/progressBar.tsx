import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Animated } from 'react-native';
import theme from '../styles/theme.styles';

interface Props {
  progress: number;
  length: number;
}

export default function CustomProgressBar(props: Props) {
  const [value] = useState(new Animated.Value(props.progress));

  useEffect(() => {
    Animated.timing(value, {
      toValue: props.progress,
      duration: 500,
    }).start();
  });

  const widthInterpolation = value.interpolate({
    inputRange: [0, props.length],
    outputRange: ['0%', '100%'],
    extrapolate: 'clamp',
  });

  return (
    <View style={[styles.container]}>
      <Animated.View style={[styles.inner, { width: widthInterpolation }]} />
      <Animated.Text style={styles.label}>
        {props.progress} / {props.length}
      </Animated.Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 15,
    backgroundColor: '#eee',
    justifyContent: 'center',
  },
  inner: {
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: '100%',
    height: 15,
    backgroundColor: theme.PROGRESS_BAR,
    opacity: 0.8,
  },
  label: {
    fontSize: 12,
    color: 'black',
    position: 'absolute',
    zIndex: 1,
    alignSelf: 'center',
  },
});
