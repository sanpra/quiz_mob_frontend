import axios from 'axios';
import { URL } from '../utils/routeConstants';
import {
  ADD_QUIZ_QUESTIONS,
  STORE_ANSWER,
  CLEAR_ANSWERS,
} from '../store/actionTypes';
import axiosInstance from '../Axios/axiosInterceptor';
import { Answers } from './questions.container';

export const getQuizQuestions = async (id: string) => {
  let response;
  await axiosInstance
    .get(`${URL}/quiz/questions/${id}`)
    .then(res => {
      response = res;
    })
    .catch(err => console.log(err));

  return response;
};

export const storeQuestions = fetchedQuestions => {
  return {
    type: ADD_QUIZ_QUESTIONS,
    payload: fetchedQuestions,
  };
};

export const storeAnswers = answer => {
  return {
    type: STORE_ANSWER,
    payload: answer,
  };
};

export const clearAnswers = () => {
  return {
    type: CLEAR_ANSWERS,
  };
};

export const submitQuiz = async (
  quizId: string,
  answers?: Array<Answers>,
  timeTaken?: number
) => {
  const payload = {
    quizId,
    quizAnswers: answers,
    timeTaken: timeTaken,
  };
  let response;
  await axiosInstance
    .post(`${URL}/result/submit`, payload)
    .then(res => {
      response = res;
    })
    .catch(err => console.log(err));
  return response;
};
