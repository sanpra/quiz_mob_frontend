import React from 'react';
import { Alert, BackHandler, View, StyleSheet, Text } from 'react-native';
import {
  Toast,
  Spinner,
  Container,
  Header,
  Left,
  Right,
  Icon,
  Button,
} from 'native-base';
import { connect } from 'react-redux';
import theme from '../styles/theme.styles';
//component
import QuestionCard from '../components/questionCard';
// actions
import {
  getQuizQuestions,
  storeQuestions,
  submitQuiz,
  clearAnswers,
} from './questions.action';

//modal
import ResultModal from '../Result/resultModalComponent';
//constants
import { i18N } from '../utils/i18N';
import { Auth } from '../Routes/routes';
import { SECONDS } from '../utils/config';

interface Props {
  navigation: any;
  questions: Array<Questions>;
  storeQuestions: (data: Array<Questions>) => void;
}

interface Quiz {
  title: string;
  description: string;
  duration: number;
}
export interface Questions {
  _id: string;
  title: string;
  type: string;
  options: Array<Options>;
}
export interface Options {
  text: string;
}
export interface Answers {
  questionId: string;
  answer: string;
}
export interface Result {
  status: string;
  userScore: number;
  userPercentage: number;
}
interface Props {
  quizDetail: Quiz;
  answers: Array<Answers>;
  clearStore: () => void;
  clearAnswers: () => void;
}
interface State {
  quizId: string;
  questions: Array<Questions>;
  showModal: boolean;
  result: Result;
  duration: number;
  timeTaken: number;
  timerRunning: boolean;
  counter: number;
  loader: boolean;
  error: boolean;
  errorMessage: string;
}

class QuestionsContainer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      quizId: '',
      questions: [],
      showModal: false,
      result: { status: '', userScore: 0, userPercentage: 0 },
      duration: 0,
      timeTaken: 0,
      timerRunning: true,
      counter: 0,
      loader: true,
      error: false,
      errorMessage: '',
    };
  }

  async componentDidMount() {
    await this.fetchQuestions();
  }
  componentWillUnmount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      BackHandler.exitApp();
      return true;
    });
  }
  fetchQuestions = async () => {
    let id = this.props.navigation.getParam('id');
    this.setState({ quizId: id, error: false });
    let response = await getQuizQuestions(id);
    if (!response) {
      this.props.navigation.navigate(Auth);
      alert(i18N.messages.logoutMessage);
    } else {
      if (response.data.status == 200) {
        await this.props.storeQuestions(response.data.data);
        this.setState({
          questions: response.data.data.questions,
          duration: response.data.data.duration,
        });
        BackHandler.addEventListener('hardwareBackPress', () => {
          this.goBack();
          return true;
        });
        response = await submitQuiz(this.state.quizId);
        if (!response) {
          this.props.navigation.navigate(Auth);
          alert(i18N.messages.logoutMessage);
        } else {
          this.setState({ loader: false });
          if (response.data.status !== 200) {
            Alert.alert('', i18N.messages.serverError, [
              { text: 'OK', onPress: () => this.props.navigation.pop() },
            ]);
          }
        }
      } else {
        this.setState({
          loader: false,
          error: true,
          errorMessage: response.data.message,
        });
        BackHandler.addEventListener('hardwareBackPress', () => {
          this.props.navigation.pop();
          return true;
        });
        Toast.show({ text: response.data.message, type: 'warning' });
      }
    }
  };

  goBack = () => {
    Alert.alert('', i18N.messages.stopQuiz, [
      { text: i18N.labels.continue },
      {
        text: i18N.labels.goBack,
        onPress: () => {
          const timeTaken =
            this.state.duration - Math.round(this.state.counter / SECONDS);
          this.submit(timeTaken);
        },
      },
    ]);
  };

  submit = async (timeTaken: number) => {
    const response = await submitQuiz(
      this.state.quizId,
      this.props.answers,
      timeTaken
    );
    if (!response) {
      this.props.navigation.navigate(Auth);
      alert(i18N.messages.logoutMessage);
    } else {
      if (response.data.status == 200) {
        let { data } = response.data;
        let result: Result = {
          status: data.result,
          userScore: data.userScore,
          userPercentage: data.userPercentage,
        };
        this.setState({ result: result, showModal: true });
      } else {
        Toast.show({ text: response.data.message, type: 'warning' });
        this.props.navigation.pop();
      }
    }
    this.props.clearAnswers();
  };

  closeModal = () => {
    this.setState({ showModal: false });
    this.props.navigation.pop();
  };

  updateCounter = time => {
    this.setState({ counter: time });
  };

  render() {
    if (this.state.showModal) {
      return (
        <ResultModal
          showModal={this.state.showModal}
          result={this.state.result}
          closeModal={this.closeModal}
        />
      );
    }
    return (
      <>
        {this.state.loader ? (
          <View style={styles.container}>
            <Spinner />
          </View>
        ) : this.state.error ? (
          <Container>
            <Header style={styles.header}>
              <Left>
                <Button transparent onPress={() => this.props.navigation.pop()}>
                  <Icon name="arrow-back" style={{ color: theme.PRIMARY }} />
                </Button>
              </Left>

              <Right />
            </Header>
            <View style={styles.container}>
              <Text>{this.state.errorMessage}</Text>
            </View>
          </Container>
        ) : (
          <QuestionCard
            questions={this.state.questions}
            submit={this.submit}
            duration={this.state.duration}
            goBack={this.goBack}
            updateCounter={this.updateCounter}
            counter={this.state.counter}
          />
        )}
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.BACKGROUND_LIGHT,
  },

  header: {
    backgroundColor: theme.BACKGROUND_LIGHT,
    alignItems: 'center',
  },
});
const mapStateToProps = state => ({
  quizDetail: state.quiz.activeQuiz,
  questions: state.quiz.activeQuiz.questions,
  answers: state.quiz.answers,
});
const mapDispatchToProps = {
  storeQuestions,
  clearAnswers,
};

export default connect(mapStateToProps, mapDispatchToProps)(QuestionsContainer);
