export default interface User {
  firstname: String;
  lastname: String;
  email: String;
  password: String;
  confirmPassword: String;
}
