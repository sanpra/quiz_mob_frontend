import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import {
  Container,
  Content,
  Header,
  Text,
  Form,
  Item as FormItem,
  Label,
  Input,
  Icon,
  Button,
  Left,
  Body,
  Right,
  Title,
  Toast,
} from 'native-base';
import theme from '../styles/theme.styles';
//
import User from './registerInterface';
// constants
import { i18N } from '../utils/i18N';

// validation functions
import {
  validateEmail,
  validateDomain,
  validateName,
  validatePassword,
} from '../common/validations';

interface Props {
  register: (user: User) => void;
  goBack: () => void;
}

interface Fields {
  value: string;
  errorStatus: boolean;
  errorMessage: string;
  securePassword?: boolean;
}

export default function RegisterComponent(props: Props) {
  const [firstname, setFirstname] = useState<Fields>({
    value: '',
    errorStatus: false,
    errorMessage: i18N.messages.requireField('Firstname'),
  });
  const [lastname, setLastname] = useState<Fields>({
    value: '',
    errorStatus: false,
    errorMessage: i18N.messages.requireField('Lastname'),
  });
  const [email, setEmail] = useState<Fields>({
    value: '',
    errorStatus: false,
    errorMessage: i18N.messages.requireField('Email'),
  });
  const [password, setPassword] = useState<Fields>({
    value: '',
    errorStatus: false,
    errorMessage: i18N.messages.requireField('Password'),
    securePassword: true,
  });
  const [confirmPassword, setConfirmedPassword] = useState<Fields>({
    value: '',
    errorStatus: false,
    errorMessage: i18N.messages.requireField('Confirm Password'),
  });

  const checkPassword = (value: string) => {
    if (password.value !== value && confirmPassword.value !== value) {
      setConfirmedPassword({
        ...confirmPassword,
        errorStatus: true,
        errorMessage: i18N.messages.passwordValidation(),
      });
    } else {
      setConfirmedPassword({
        ...confirmPassword,
        value: value,
        errorStatus: false,
      });
    }
  };

  const validate = (key: string, value: string) => {
    switch (key) {
      case 'firstname':
        let validFirstName = validateName(value);
        if (validFirstName) {
          setFirstname({ ...firstname, value: value, errorStatus: false });
        } else {
          if (value.length >= 50) {
            setFirstname({
              ...firstname,
              errorStatus: true,
              errorMessage: i18N.messages.maxLengthValidation('Firstname', 50),
            });
          } else if (value == '') {
            setFirstname({
              ...firstname,
              errorStatus: true,
              errorMessage: i18N.messages.requireField('Firstname'),
            });
          } else {
            setFirstname({
              ...firstname,
              errorStatus: true,
              errorMessage: i18N.messages.alphanumericValidation('Firstname'),
            });
          }
        }
        break;
      case 'lastname':
        let validLastName = validateName(value);
        if (validLastName) {
          setLastname({ ...lastname, value: value, errorStatus: false });
        } else {
          if (value.length >= 50) {
            setLastname({
              ...lastname,
              errorStatus: true,
              errorMessage: i18N.messages.maxLengthValidation('Lastname', 50),
            });
          } else if (value == '') {
            setLastname({
              ...lastname,
              errorStatus: false,
              // errorMessage: i18N.messages.requireField('Lastname'),
            });
          } else {
            setLastname({
              ...lastname,
              errorStatus: true,
              errorMessage: i18N.messages.alphanumericValidation('Lastname'),
            });
          }
        }

        break;
      case 'email':
        let validEmail = validateEmail(value);
        if (validEmail) {
          let validDomain = validateDomain(value);
          if (validDomain) {
            setEmail({ ...email, value: value, errorStatus: false });
          } else {
            setEmail({
              ...email,
              errorStatus: true,
              errorMessage: i18N.messages.invalidField('Domain'),
            });
          }
        } else {
          setEmail({
            ...email,
            errorStatus: true,
            errorMessage: i18N.messages.invalidField('Email'),
          });
        }
        break;
      case 'password':
        let validPassword = validatePassword(value);
        if (validPassword.valid) {
          setPassword({ ...password, value: value, errorStatus: false });
        } else {
          setPassword({
            ...password,
            value: value,
            errorStatus: true,
            errorMessage: validPassword.message,
          });
        }
        checkPassword(value);
        break;
      default:
        return false;
    }
  };

  const onRegisterValidation = () => {
    if (
      firstname.value == '' &&
      // lastname.value == '' &&
      email.value == '' &&
      password.value == '' &&
      confirmPassword.value == ''
    ) {
      setFirstname({ ...firstname, errorStatus: true });
      // setLastname({ ...lastname, errorStatus: true });
      setEmail({ ...email, errorStatus: true });
      setPassword({ ...password, errorStatus: true });
      setConfirmedPassword({ ...confirmPassword, errorStatus: true });
    } else if (
      !firstname.errorStatus &&
      !lastname.errorStatus &&
      !email.errorStatus &&
      !password.errorStatus &&
      !confirmPassword.errorStatus &&
      firstname.value !== '' &&
      email.value !== '' &&
      password.value !== '' &&
      confirmPassword.value !== ''
    ) {
      let user = {
        firstname: firstname.value,
        lastname: lastname.value,
        email: email.value,
        password: password.value,
        confirmPassword: confirmPassword.value,
      };
      props.register(user);
    } else {
      Toast.show({ text: i18N.messages.emptyForm });
    }
  };

  return (
    <Container>
      <Header style={styles.header}>
        <Left>
          <Button transparent onPress={() => props.goBack()}>
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>{i18N.headers.createAccount}</Title>
        </Body>
        <Right />
      </Header>

      <Content>
        <Form style={styles.formContainer}>
          {/* firstname */}
          <FormItem
            floatingLabel
            style={[styles.textInput]}
            success={firstname.value.length > 2 ? true : false}
            error={firstname.errorStatus}
          >
            <Label>{i18N.inputFieldLabels.firstname}</Label>
            <Input
              onChangeText={firstname => validate('firstname', firstname)}
              returnKeyLabel={'Next'}
              returnKeyType={'next'}
            />
          </FormItem>
          {firstname.errorStatus && (
            <Text style={styles.error}>{firstname.errorMessage}</Text>
          )}
          {/* lastname */}
          <FormItem
            floatingLabel
            style={[styles.textInput]}
            success={lastname.value.length > 2 ? true : false}
            error={lastname.errorStatus}
          >
            <Label>{i18N.inputFieldLabels.lastname}</Label>
            <Input
              onChangeText={lastname => validate('lastname', lastname)}
              returnKeyLabel={'Next'}
              returnKeyType={'next'}
            />
          </FormItem>
          {lastname.errorStatus && (
            <Text style={styles.error}>{lastname.errorMessage}</Text>
          )}
          {/* email */}
          <FormItem
            floatingLabel
            style={styles.textInput}
            success={email.value.length > 2 ? true : false}
            error={email.errorStatus}
          >
            <Label>{i18N.inputFieldLabels.email}</Label>
            <Input
              onChangeText={email => validate('email', email)}
              keyboardType={'email-address'}
              returnKeyLabel={'Next'}
              returnKeyType={'next'}
            />
          </FormItem>
          {/* <Text style={styles.info}>{emailInfoMessage}</Text> */}
          {email.errorStatus && (
            <Text style={styles.error}>{email.errorMessage}</Text>
          )}

          <FormItem
            floatingLabel
            style={styles.textInput}
            error={password.errorStatus}
            success={
              password.value.length >= 1 && !password.errorStatus ? true : false
            }
          >
            <Label>{i18N.inputFieldLabels.password}</Label>
            <Input
              secureTextEntry={password.securePassword}
              returnKeyLabel={'Next'}
              returnKeyType={'next'}
              onChangeText={password => validate('password', password)}
              onBlur={() => setPassword({ ...password, securePassword: true })}
            />
            <Icon
              name="eye"
              style={styles.securePasswordIcon}
              android={password.securePassword ? 'md-eye-off' : 'md-eye'}
              ios={password.securePassword ? 'ios-eye-off' : 'ios-eye'}
              onPress={() =>
                setPassword({
                  ...password,
                  securePassword: !password.securePassword,
                })
              }
            />
          </FormItem>
          {password.errorStatus && (
            <Text style={styles.error}>{password.errorMessage}</Text>
          )}

          <FormItem
            floatingLabel
            style={styles.textInput}
            error={confirmPassword.errorStatus}
            success={
              confirmPassword.value.length >= 1 && !confirmPassword.errorStatus
                ? true
                : false
            }
          >
            <Label>{i18N.inputFieldLabels.confirmPassword}</Label>
            <Input
              secureTextEntry={true}
              returnKeyLabel={'Done'}
              returnKeyType={'done'}
              onChangeText={confirmPassword => checkPassword(confirmPassword)}
              contextMenuHidden={true}
              onSubmitEditing={() => onRegisterValidation()}
            />
          </FormItem>
          {confirmPassword.errorStatus && (
            <Text style={styles.error}>{confirmPassword.errorMessage}</Text>
          )}

          <Button
            style={styles.buttonRegister}
            rounded
            onPress={() => onRegisterValidation()}
          >
            <Text style={styles.buttonRegisterText}>
              {i18N.labels.registerBtn}
            </Text>
          </Button>
        </Form>
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: theme.PRIMARY,
  },
  formContainer: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    padding: 10,
  },
  textInput: {
    padding: 10,
    // borderBottomColor: '#ddd',
  },
  buttonRegister: {
    marginTop: 25,
    alignSelf: 'center',
    backgroundColor: theme.SECONDARY,
    paddingHorizontal: 15,
  },
  buttonRegisterText: {
    alignSelf: 'center',
    fontSize: 16,
  },
  error: {
    paddingHorizontal: 20,
    color: theme.ERROR,
  },
  info: {
    paddingHorizontal: 20,
    fontStyle: 'italic',
    color: theme.SECONDARY_INFO,
  },
  securePasswordIcon: {
    color: theme.ICON_DARK,
  },
});
