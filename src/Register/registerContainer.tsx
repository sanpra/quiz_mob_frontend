import React from 'react';
import { Toast } from 'native-base';
import { Alert, BackHandler } from 'react-native';
// Services
import { registerUser } from './registerService';

// Login component
import RegisterComponent from './registerComponent';

// route names
import User from './registerInterface';
import { i18N } from '../utils/i18N';

interface Props {
  navigation: any;
}

class RegisterContainer extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.goBack();
      return true;
    });
  }

  componentWillUnmount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      BackHandler.exitApp();
      return true;
    });
  }

  goBack = () => {
    Alert.alert(i18N.alertGoBack.header, i18N.alertGoBack.message, [
      { text: i18N.alertGoBack.optionContinue },
      {
        text: i18N.alertGoBack.optionStop,
        onPress: () => this.props.navigation.pop(),
      },
    ]);
  };

  register = async (user: User) => {
    let res = await registerUser(user);
    if (res.data.status == 200) {
      Toast.show({ text: res.data.message, type: 'success', duration: 3000 });
      this.props.navigation.pop();
    } else if (res.data.status == 400) {
      Toast.show({ text: res.data.message, type: 'danger' });
    } else {
      Toast.show({ text: res.data.message, type: 'danger' });
    }
  };

  render() {
    return <RegisterComponent register={this.register} goBack={this.goBack} />;
  }
}

export default RegisterContainer;
