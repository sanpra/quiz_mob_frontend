import axios from 'axios';

import User from './registerInterface';

// URL constants
import { URL, URL_USER, REGISTER_USER } from '../utils/routeConstants';

export const registerUser = async (user: User) => {
  let response;
  await axios
    .post(`${URL}/user/register`, user)
    .then(res => {
      response = res;
    })
    .catch(err => console.log('err = ', err));
  console.log(response);
  return response;
};
