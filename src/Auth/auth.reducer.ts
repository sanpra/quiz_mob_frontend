// import action types
import { AUTH_USER } from '../store/actionTypes';
import { REHYDRATE } from 'redux-persist';

const initialState = {
  authUser: {},
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case REHYDRATE:
      if (action.payload) {
        return {
          ...state,
          authUser: action.payload.auth.authUser
        };
      } else {
        return { ...state };
      }
    case AUTH_USER:
      return { ...state, authUser: action.payload };
    default:
      return { ...state };
  }
};

export default authReducer;
