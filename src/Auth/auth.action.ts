import axios from 'axios';
import { URL } from '../utils/routeConstants';

import { AUTH_USER, CLEAR_SESSION } from '../store/actionTypes';

export const storeAuthUser = userDetails => {
  return {
    type: AUTH_USER,
    payload: userDetails,
  };
};

export const authUser = async (email: string, password: string) => {
  let response;
  const payload = {
    email,
    password,
  };

  await axios
    .post(`${URL}/auth`, payload)
    .then(res => {
      response = res;
    })
    .catch(err => {
      throw err;
    });
  return response;
};

export const logout = async id => {
  let response;
  await axios
    .put(`${URL}/user/${id}`)
    .then(res => {
      response = res;
    })
    .catch(err => {
      throw err;
    });
  return response;
};

export const clearStore = () => {
  return {
    type: CLEAR_SESSION,
  };
};
