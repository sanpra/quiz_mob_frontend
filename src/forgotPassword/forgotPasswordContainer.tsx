import React from 'react';
import { Toast } from 'native-base';

import ForgotPasswordComponent from './forgotPasswordComponent';
import { sendEmail } from './forgotPassword.actions';
interface Props {
  navigation: any;
}

class ForgotPasswordContainer extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  goBack = () => {
    this.props.navigation.pop();
  };

  sendLink = async (email: string) => {
    let response = await sendEmail(email);
    if (response.data.status === 401) {
      Toast.show({ text: response.data.message, type: 'danger' });
    } else if (response.data.status == 200) {
      Toast.show({
        text: response.data.message,
        type: 'success',
        duration: 3000,
      });
      this.props.navigation.pop();
    } else {
      Toast.show({ text: response.data.message, type: 'warning' });
    }
  };

  render() {
    return (
      <ForgotPasswordComponent sendLink={this.sendLink} goBack={this.goBack} />
    );
  }
}

export default ForgotPasswordContainer;
