import axios from 'axios';
import { URL } from '../utils/routeConstants';

export const sendEmail = async (email: string) => {
  let response;
  const payload = { email };
  await axios
    .post(`${URL}/auth/reqresetpassword`, payload)
    .then(res => {
      response = res;
    })
    .catch(err => console.log(err));
  console.log(response);
  return response;
};
