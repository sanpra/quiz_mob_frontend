import React, { useState } from 'react';
import { StyleSheet, Text } from 'react-native';
import {
  Container,
  Header,
  Button,
  Icon,
  Title,
  Body,
  Left,
  Content,
  Input,
  Label,
  Item as FormItem,
  Toast,
} from 'native-base';
import { i18N } from '../utils/i18N';
import { validateEmail, validateDomain } from '../common/validations';
import theme from '../styles/theme.styles';

interface Props {
  goBack: () => void;
  sendLink: (email: String) => void;
}

export default function ForgotPasswordComponent(props: Props) {
  const [email, setEmail] = useState({
    value: '',
    errorStatus: false,
    errorMessage: '',
  });

  const checkEmail = email => {
    let validEmail = validateEmail(email);
    if (validEmail) {
      let validDomain = validateDomain(email);
      if (validDomain) {
        props.sendLink(email);
      } else {
        Toast.show({
          text: i18N.messages.invalidField('Domain'),
          type: 'danger',
        });
      }
    } else {
      Toast.show({ text: i18N.messages.invalidField('Email'), type: 'danger' });
    }
  };

  return (
    <Container>
      <Header style={styles.header}>
        <Left>
          <Button transparent>
            <Icon name="arrow-back" onPress={() => props.goBack()} />
          </Button>
        </Left>
        <Body>
          <Title>{i18N.headers.forgotPassword}</Title>
        </Body>
        {/* <Right /> */}
      </Header>
      <Content contentContainerStyle={styles.mainContainer}>
        <FormItem bordered>
          <Label>{i18N.inputFieldLabels.email}</Label>
          <Input
            style={styles.textinput}
            onChangeText={input => setEmail({ ...email, value: input })}
            keyboardType={'email-address'}
            returnKeyLabel={'Done'}
            returnKeyType={'done'}
          />
        </FormItem>
        {email.value.length == 0 ? (
          <Text>{i18N.messages.enterEmailMessage}</Text>
        ) : (
          <Button style={styles.button} onPress={() => checkEmail(email.value)}>
            <Text style={styles.buttonText}>{i18N.labels.sendLink}</Text>
          </Button>
        )}
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: theme.PRIMARY,
  },
  mainContainer: {
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  textinput: {
    // margin: 10,
  },
  button: {
    margin: 10,
    padding: 10,
    alignSelf: 'center',
    backgroundColor: theme.SECONDARY,
  },
  buttonText: {
    color: theme.TEXT_LIGHT,
    alignSelf: 'center',
    fontSize: 16,
  },
});
