import React from 'react';
import { StyleSheet, StatusBar, Platform, AsyncStorage } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Root } from 'native-base';

import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

// root navigator
import { rootNavigator } from './src/Routes';

// redux store
import store, { persistedStore } from './src/store';

// redux store provide
import { Provider } from 'react-redux';

import { PersistGate } from 'redux-persist/integration/react';

interface Props {}
interface State {
  isLoggedIn: string;
  isReady: boolean;
}

class App extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoggedIn: 'false',
    };
  }
  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('./node_modules/native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('./node_modules/native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
    await AsyncStorage.getItem('LoggedIn')
      .then(res => {
        this.setState({ isLoggedIn: res });
      })
      .catch(err => console.log(err));
  }

  render() {
    Platform.OS === 'ios'
      ? StatusBar.setTranslucent(false)
      : StatusBar.setTranslucent(true);

    const AppNavigator = rootNavigator(this.state.isLoggedIn);
    return (
      <SafeAreaView style={styles.safeAreaContainer}>
        <Provider store={store}>
          <PersistGate persistor={persistedStore} loading={null}>
            <Root>
              <AppNavigator />
            </Root>
          </PersistGate>
        </Provider>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: StatusBar.currentHeight,
  },
});

export default App;
